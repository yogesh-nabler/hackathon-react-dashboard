import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

const state = {
  labels: ['impresson', 'clicks', 'transations'],
  datasets: [
    {
      label: 'Rainfall',
      backgroundColor: 'rgba(75,192,192,1)',
      borderColor: 'rgba(0,0,0,1)',
      borderWidth: 2,
      data: [65, 59, 80]
    }
  ]
}

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      actualData: null,
      dummyData: [5, "2018-01-01", "Campaign_1", 94506, 89777, 80622, "adwords", 6, "2018-01-01", "Campaign_2", 55571, 32845, 29932, "adwords", 7, "2018-01-01", "Campaign_3", 88208, 84694, 22375, "adwords", 8, "2018-01-01", "Campaign_4", 66021, 20093, 7814, "adwords", 9, "2018-01-01", "Campaign_5", 59432, 33992, 24413, "adwords", 10, "2018-01-01", "Campaign_6", 82275, 11430, 8466, "adwords", 11, "2018-01-01", "Campaign_7", 91467, 53405, 5317, "adwords", 12, "2018-01-01", "Campaign_8", 93672, 50031, 10057, "adwords"]
    };
  }

  componentDidMount() {
    const url = "https://hvdz5tqo6d.execute-api.ap-south-1.amazonaws.com/hackathon-test/hackathon-2021"
    fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log(JSON.stringify(data));
        this.setState({ actualData: data })
      })
      .catch(error => {
        console.log(error)
      });
  }


  render() {
    let data = [];
    if (this.state.actualData) {
      data = this.state.actualData;
    } else {
      data = this.state.dummyData;
    }
    let slicedArray = [];
    let impression = 0;
    let clicks = 0;
    let transations = 0;
    for (let i = 0; i < data.length; i += 7) {
      let chuckArray = data.slice(i, i + 7);

      console.log(i)
      console.log(chuckArray)
      slicedArray.push(chuckArray)
    }
    console.log(slicedArray)

    slicedArray.map((item, itemIndex) => {
      impression += item[3]
      clicks += item[4]
      transations += item[5]
      console.log(impression, clicks, transations)
    })

    return (
      <div id="app-main-container" >
        <Bar
          // data={state}
          data={
            {
              labels: ['impresson', 'clicks', 'transations'],
              datasets: [
                {
                  label: 'adwords',
                  backgroundColor: 'rgba(75,192,192,1)',
                  borderColor: 'rgba(0,0,0,1)',
                  borderWidth: 2,
                  data: [impression, clicks, transations]
                }
              ]
            }
          }
          options={{
            title: {
              display: true,
              text: 'Advt',
              fontSize: 20
            },
            legend: {
              display: true,
              position: 'right'
            }
          }}
        />
      </div>
    );
  }
}

export default App;
